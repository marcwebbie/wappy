#include "WelcomeDialog.h"

WelcomeDialog::WelcomeDialog(QWidget *parent) :
    QDialog(parent)
{
    this->resize(400, 100);
    this->m_chosenUrl = "";

    // Build url layout
    this->m_urlLabel = new QLabel("Url: ");
    this->m_urlText = new QLineEdit("http://");
    this->m_goToUrlButton = new QPushButton("Go");

    this->m_urlLayout = new QHBoxLayout();
    this->m_urlLayout->addWidget(m_urlLabel);
    this->m_urlLayout->addWidget(m_urlText);
    this->m_urlLayout->addWidget(m_goToUrlButton);

    // Build favorites
    this->m_openBookmarksButton = new QPushButton("Open Bookmarks");

    // Build main layout
    this->m_mainLayout = new QVBoxLayout();
    this->m_mainLayout->addLayout(m_urlLayout);
    this->m_mainLayout->addWidget(m_openBookmarksButton);

    this->setLayout(m_mainLayout);

    // Connections
    connect(m_goToUrlButton, SIGNAL(clicked()), this, SLOT(setUrlString()));
}

QString WelcomeDialog::getChosenUrl()
{
    return m_chosenUrl;
}

void WelcomeDialog::setUrlString()
{
    m_chosenUrl = QString(m_urlText->text());
    this->close();
}
