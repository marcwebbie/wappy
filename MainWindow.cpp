#include "MainWindow.h"

MainWindow::MainWindow() :
    curr_page(new QWebPage(this))
{
    welcomeDialog = new WelcomeDialog(this);
    welcomeDialog->exec();

    chosenUrl = welcomeDialog->getChosenUrl();
    if(chosenUrl == "" || chosenUrl == "http://"){
        exit(2);
    }
    else{
        this->resize(1064, 500);
        this->settings()->setAttribute(QWebSettings::PluginsEnabled, true);

        this->load(QUrl(chosenUrl)); // FIXME: No error handling
        connect(curr_page, SIGNAL(loadFinished(bool)), this, SLOT(addTitle(bool)));
    }
}

QString MainWindow::askUrl()
{
    bool ok;
    QString text = QInputDialog::getText(this, tr("Build a web application"),
                                         tr("Address: "), QLineEdit::Normal,
                                         "http://", &ok);
    if (ok && !text.isEmpty())
        return text;
    else
        return "about:404";
}

void MainWindow::addTitle(bool)
{
    this->setWindowTitle(curr_page->view()->windowTitle());
}
