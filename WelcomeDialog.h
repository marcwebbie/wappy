#ifndef WELCOMEDIALOG_H
#define WELCOMEDIALOG_H

#include <QWidget>
#include <QDialog>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QPushButton>
#include <QLineEdit>
#include <QLabel>

class WelcomeDialog : public QDialog
{
    Q_OBJECT
public:
    explicit WelcomeDialog(QWidget *parent = 0);
    QString getChosenUrl();
    
signals:
    
public slots:
    void setUrlString();
    
private:
    QVBoxLayout* m_mainLayout;
    QHBoxLayout* m_urlLayout;

    QLineEdit* m_urlText;
    QLabel* m_urlLabel;

    QPushButton* m_openBookmarksButton;
    QPushButton* m_goToUrlButton;

    QString m_chosenUrl;
};

#endif // WELCOMEDIALOG_H
