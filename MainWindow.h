#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QWidget>
#include <QWebView>
#include <QString>
#include <QInputDialog>
#include <QApplication>
#include "WelcomeDialog.h"

class MainWindow : public QWebView
{
    Q_OBJECT
public:
    MainWindow();

private slots:
    QString askUrl();
    void addTitle(bool);

private:
    QWebPage* curr_page;
    QString chosenUrl;

    WelcomeDialog* welcomeDialog;
};
\
#endif // MAINWINDOW_H
